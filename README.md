<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重要的</font></font></h2><a id="user-content-important" class="anchor" aria-label="永久链接：重要" href="#important"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用 Studio 3T，请在</font></font><a href="https://studio3t.com/feedback/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Studio 3T 反馈</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">页面上提交任何反馈。如果您使用的是 Studio 3T Free，可以在</font></font><a href="https://community.studio3t.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3T 社区中通过</font></font></a><font style="vertical-align: inherit;"></font><a href="https://community.studio3t.com/c/studio-3t-free/13" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">专门的免费部分</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">进行讨论</font><font style="vertical-align: inherit;">。 Studio 3T 不会监视此存储库中的 Studio 3T 问题。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T 开发结束</font></font></h1><a id="user-content-end-of-robo-3t-development" class="anchor" aria-label="永久链接：Robo 3T 开发结束" href="#end-of-robo-3t-development"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Studio 3T 不再开发 Robo 3T。 Studio 3T 建议寻找 MongoDB GUI 客户端的用户尝试</font></font><a href="https://studio3t.com/free" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Studio 3T Free</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，这是 Studio 3T 工具的永久免费版本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://blog.robomongo.org/studio3t-free/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在Robo 3T 博客</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上了解有关更改的更多信息</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T 的最新版本是版本 1.4.4，可从以下链接下载：</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><a href="https://download.studio3t.com/robomongo/windows/robo3t-1.4.4-windows-x86_64-e6ac9ec5.zip" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T Windows .zip</font></font></a></p>
</li>
<li>
<p dir="auto"><a href="https://download.studio3t.com/robomongo/windows/robo3t-1.4.4-windows-x86_64-e6ac9ec5.exe" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T Windows.exe</font></font></a></p>
</li>
<li>
<p dir="auto"><a href="https://download.studio3t.com/robomongo/mac/robo3t-1.4.4-darwin-x86_64-e6ac9ec.dmg" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">机器人3T Mac</font></font></a></p>
</li>
<li>
<p dir="auto"><a href="https://download.studio3t.com/robomongo/linux/robo3t-1.4.4-linux-x86_64-e6ac9ec.tar.gz" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">机器人3T Linux</font></font></a></p>
</li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://github.com/Studio3T/robomongo/latest"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以及来自Robo 3T 存储库的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">源代码</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Studio 3T 衷心感谢自 2017 年收购以来使用和支持该应用程序的 Robo 3T 社区。该存储库、网站和博客将保持在线状态，并可用于维护最有影响力的 MongoDB 之一的记录当时的客户。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关于机器人3T</font></font></h1><a id="user-content-about-robo-3t" class="anchor" aria-label="永久链接：关于 Robo 3T" href="#about-robo-3t"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="http://www.robomongo.org" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（以前称为 Robomongo*）是一个以 shell 为中心的跨平台 MongoDB 管理工具。与大多数其他 MongoDB 管理 UI 工具不同，Robo 3T 将实际的</font></font><code>mongo</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">shell 嵌入到选项卡式界面中，可以访问 shell 命令行以及 GUI 交互。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最新的稳定版本</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T 1.4</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">嵌入了</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MongoDB 4.2</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> shell。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">博客：      </font></font><a href="http://blog.robomongo.org/robo-3t-1-4/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ://blog.robomongo.org/robo-3t-1-4/
下载： https: </font></font><a href="https://robomongo.org/download" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//robomongo.org/download</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
所有版本：</font></font><a href="https://github.com/Studio3T/robomongo/releases"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/Studio3T/robomongo/releases</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
观看：</font></font><a href="https://www.youtube.com/channel/UCM_7WAseRWeeiBikExppstA" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T Youtube 频道</font></font></a><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
关注： https: </font></font><a href="https://twitter.com/Robomongo" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//twitter.com/Robomongo</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">嵌入式 MongoDB shell 历史记录：</font></font></strong><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 
Robo 3T 1.4 -&gt; MongoDB 4.2 </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
Robo 3T 1.3 -&gt; MongoDB 4.0 </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
Robo 3T 1.1 -&gt; MongoDB 3.4 </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
Robo 3T 0.9 -&gt; MongoDB 3.2 </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
Robo 3T 0.8.x -&gt; MongoDB 2.4.0</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">* </font></font><a href="https://studio3t.com/press/3t-software-labs-acquires-robomongo-the-most-widely-used-mongodb-tool/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robomongo 已被 3T 收购</font></font></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最新的 Robo 3T 1.4 有什么新功能？</font></font></h1><a id="user-content-whats-new-in-latest-robo-3t-14" class="anchor" aria-label="永久链接：最新的 Robo 3T 1.4 有什么新功能？" href="#whats-new-in-latest-robo-3t-14"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新功能：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">蒙戈外壳4.2升级</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持 Ubuntu 20.04、macOS Big Sur 和 macOS 10.15 (Catalina)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SSH：Windows 和 macOS 上的 ECDSA 和 Ed25519 密钥支持（问题 #1719、#1530、#1590）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手动指定可见数据库（问题#1696、#1368、#389）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新的欢迎选项卡 - 使用 QtWebEngine 嵌入 Chromium（仅限 Windows、macOS）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从旧版本导入键：autoExpand、lineNumbers、debugMode 和 shellTimeoutSec</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">改进：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Qt 升级（v5.12.8 - 2020 年 4 月，仅限 Windows 和 macOS）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenSSL 升级（v1.1.1f - 2020 年 3 月，仅限 Windows 和 macOS）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">libssh2 升级（v1.9.0 - 2019 年 6 月，仅限 Windows 和 macOS）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库浏览器部分的默认宽度较小 (#1556)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">记住数据库浏览器部分的大小</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过命令行修复之前损坏的 IPv6 支持：robo3t --ipv6</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复在选项卡式结果窗口中使用分页时的崩溃问题 (#1661)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复 DocumentDB 中损坏的分页 (#1694)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">身份验证数据库选项未正确使用 (#1696)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">添加/编辑索引操作已修复（重写）（#1692）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">扩展管理员用户时崩溃 (#1728)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">shell 超时后无法运行查询 (#1529)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复选项卡式结果视图中损坏的 F2、F3、F4 快捷键</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个新连接窗口的一次重新排序限制，以防止数据丢失（macOS，#1790）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修复在服务器无法访问的情况下执行新 shell 选项卡时的崩溃问题</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持的平台</font></font></h1><a id="user-content-supported-platforms" class="anchor" aria-label="永久链接：支持的平台" href="#supported-platforms"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意：本节适用于 Robo 3T，它直接取决于 MongoDB 支持的内容</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
（请参阅： https: </font></font><a href="https://docs.mongodb.com/manual/administration/production-notes/#prod-notes-supported-platforms" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//docs.mongodb.com/manual/administration/development-notes/#prod-notes-supported-platforms</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">）</font></font></p>
<table>
<thead>
<tr>
<th align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MongoDB 版本</font></font></th>
<th align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MongoDB 云平台</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4.2</font></font></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">蒙戈·阿特拉斯</font></font></td>
</tr>
<tr>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4.0</font></font></td>
<td align="left"></td>
</tr>
<tr>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.6</font></font></td>
<td align="left"></td>
</tr>
</tbody>
</table>
<table>
<thead>
<tr>
<th align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视窗</font></font></th>
<th align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苹果</font></font></th>
<th align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Windows 64 位 10</font></font></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mac OS X 11（大苏尔）</font></font></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux Ubuntu 20.04 64 位</font></font></td>
</tr>
<tr>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Windows 64 位 8.1</font></font></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mac OS X 10.15（卡特琳娜）</font></font></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux Ubuntu 18.04 64 位</font></font></td>
</tr>
<tr>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Windows 64 位 7</font></font></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mac OS X 10.14（莫哈韦沙漠）</font></font></td>
<td align="left"></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献！</font></font></h1><a id="user-content-contribute" class="anchor" aria-label="永久链接：贡献！" href="#contribute"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码贡献</font></font></h3><a id="user-content-code-contributions" class="anchor" aria-label="永久链接：代码贡献" href="#code-contributions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在此处查看所有文档： https: </font></font><a href="https://github.com/Studio3T/robomongo/wiki"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//github.com/Studio3T/robomongo/wiki</font></font></a></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一些重要的文档：</font></font></strong></p>
<ul dir="auto">
<li><a href="https://github.com/Studio3T/robomongo/wiki/Robo-3T-Schematics:-Build,-Class-and-UI-Diagrams#1-build-diagram"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建图</font></font></a></li>
<li><a href="https://github.com/Studio3T/robomongo/wiki/Static-Code-Analysis"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">静态代码分析</font></font></a></li>
<li><a href="https://github.com/Studio3T/robomongo/wiki/Feature-Spec"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T 功能规格</font></font></a></li>
<li><a href="https://github.com/Studio3T/robomongo/blob/master/docs/Debug.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">调试</font></font></a></li>
<li><a href="https://github.com/Studio3T/robomongo/tree/master/schematics"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原理图</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">始终欢迎代码贡献！只需尝试遵循我们的预提交检查和编码风格：</font></font></p>
<ul dir="auto">
<li><a href="https://github.com/paralect/robomongo/wiki/Robomongo-Code-Quality"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T 代码质量</font></font></a></li>
<li><a href="https://github.com/Studio3T/robomongo/wiki/Robomongo-Cplusplus-11,-14-Transition-Guide"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T C++11/14 过渡指南</font></font></a></li>
<li><a href="https://github.com/paralect/robomongo/wiki/Robomongo-Coding-Style"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T 编码风格</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您打算做出贡献，请创建一个 Github 问题（或对相关的现有问题发表评论），以便我们可以帮助协调即将发布的计划。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">拉取请求 (PR) 通常应该针对离散问题（即每个 PR 一个问题），并且可以干净地合并到当前主分支。如果您可以确认已完成哪些测试（特定操作系统目标和 MongoDB 版本，如果适用），也会很有帮助。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">功能分支的常用命名方法是</font></font><code>issue-###</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.在提交消息/拉取请求描述中包含问题编号，以将 PR 链接到原始问题。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">例如：
</font></font><code>#248: updated QScintilla to 2.4.8 for retina display support".</code></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">测试</font></font></h3><a id="user-content-testing" class="anchor" aria-label="永久链接：测试" href="#testing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/Studio3T/robomongo/wiki/Unit-Tests"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单元测试</font></font></a></li>
<li><a href="/Studio3T/robomongo/blob/master/wiki/Tests"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手动测试</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在此处查看所有文档： https: </font></font><a href="https://github.com/Studio3T/robomongo/wiki"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//github.com/Studio3T/robomongo/wiki</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">建议功能</font></font></h3><a id="user-content-suggest-features" class="anchor" aria-label="永久链接：建议功能" href="#suggest-features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">始终欢迎新功能建议或 UI 改进。
</font></font><a href="https://github.com/paralect/robomongo/issues/new"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 github 上创建新功能请求</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该项目由开源志愿者提供支持，因此我们只有有限的开发资源来满足所有请求。我们一定会尽最大努力取得进展（特别是对于那些拥有强烈社区支持的人）。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载</font></font></h1><a id="user-content-download" class="anchor" aria-label="永久链接： 下载" href="#download"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以从我们的网站下载经过测试的 macOS、Windows 和 Linux 安装程序包：</font></font><a href="http://www.robomongo.org" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">www.robomongo.org</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font></font></h1><a id="user-content-support" class="anchor" aria-label="永久链接： 支持" href="#support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Robo 3T 是一个由志愿者驱动的开源项目。我们会尽快回答您的问题，但请耐心等待:)。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">你可以：</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><a href="https://github.com/paralect/robomongo/issues"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Github 问题队列中创建一个新问题</font></font></a></p>
</li>
<li>
<p dir="auto"><a href="https://gitter.im/paralect/robomongo" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加入 Gitter 上的开发者讨论</font></font></a></p>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></h1><a id="user-content-license" class="anchor" aria-label="永久链接：许可证" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">版权所有 2014-2021 </font></font><a href="https://studio3t.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3T 软件实验室有限公司</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。版权所有。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该程序是免费软件：您可以根据自由软件基金会发布的 GNU 通用公共许可证版本 3 的条款重新分发和/或修改它。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分发此程序的目的是希望它有用，但不提供任何保证；甚至没有适销性或特定用途适用性的默示保证。有关更多详细信息，请参阅 GNU 通用公共许可证。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您应该随该程序一起收到 GNU 通用公共许可证的副本。如果没有，请参阅</font></font><a href="http://www.gnu.org/licenses/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">http://www.gnu.org/licenses/</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
</article></div>
